﻿*******************************************************************************
** Component Information
*******************************************************************************

	Component:              ForceLogin
	Author:                 Jonathan Hult
	Website:                http://jonathanhult.com
	Last Updated On:        build_3_20130516

*******************************************************************************
** Overview
*******************************************************************************

	This component implements an alterUserCredentials filter. It forces all 
	users to be logged into WebCenter Content in order to call any service.
	
	Filters:
	alterUserCredentials - This filter gets called during every service call.
		
	Preference Prompts:
	ForceLogin_ComponentEnabled - Boolean determining if the component 
	functionality is enabled.
	ForceLogin_AllowedServices - A comma separated list of services which 
	are allowed to be run by anonymous users.
	
*******************************************************************************
** COMPATIBILITY WARNING
*******************************************************************************

	This component was built upon and tested on the version listed below, 
	this is the only version it is "known" to work on, but it may well work 
	on older/newer versions.
	-10.1.3.5.1 (111229) (Build: 7.2.4.105)

*******************************************************************************
** HISTORY
*******************************************************************************
	
	build_3_20130516
		- Added LOGOUT service to allowed services list
	build_2_20130506
		- Added allowed services functionality
	build_1_20130415
		- Initial component release