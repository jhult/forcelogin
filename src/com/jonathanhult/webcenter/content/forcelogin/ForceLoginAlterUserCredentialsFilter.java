package com.jonathanhult.webcenter.content.forcelogin;

import static intradoc.common.Report.m_verbose;
import static intradoc.shared.SharedObjects.getEnvValueAsBoolean;
import static intradoc.shared.SharedObjects.getEnvValueAsList;
import intradoc.common.ExecutionContext;
import intradoc.common.Report;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.Workspace;
import intradoc.server.Service;
import intradoc.shared.FilterImplementor;

import java.util.List;

public class ForceLoginAlterUserCredentialsFilter implements FilterImplementor {

	private final String TRACE_SECTION = "forcelogin";
	private final String COMPONENT_ENABLED = "ForceLogin_ComponentEnabled";
	private final String ALLOWED_SERVICES = "ForceLogin_AllowedServices";
	private final String IS_ALLOW_ANONYMOUS = "IsAllowAnonymous";
	private final String LOGOUT = "LOGOUT";

	public int doFilter(final Workspace ws, final DataBinder binder, final ExecutionContext ctx) throws DataException, ServiceException {
		try {
			traceVerbose("Start alterUserCredentials doFilter for ForceLogin");

			// Only force login if component preference prompt is enabled
			if (getEnvValueAsBoolean(COMPONENT_ENABLED, false)) {
				trace("ForceLogin component enabled");

				// If this is a service call, check to ensure user is logged in
				if (ctx instanceof Service) {
					final Service service = ((Service) ctx);
					final String serviceName = service.getServiceData().m_name;
					trace("Service: " + serviceName);

					@SuppressWarnings("unchecked")
					final List<String> allowedServices = getEnvValueAsList(ALLOWED_SERVICES);
					allowedServices.add(LOGOUT);
					traceVerbose("allowedServices: " + allowedServices);

					// If service is set as an allowed service, let it proceed
					// as normal
					if (allowedServices != null && !allowedServices.contains(serviceName)) {
						final boolean isLoggedIn = service.isLoggedIn();
						trace("isLoggedIn: " + isLoggedIn);

						// If user is not logged in, redirect to login page
						if (!isLoggedIn) {
							// Disable anonymous access via parameter
							binder.putLocal(IS_ALLOW_ANONYMOUS, "0");

							trace("Forcing login via checkForceLogin method");
							service.getHttpImplementor().checkForceLogin();
						}
					}
				}
			}
		} finally {
			traceVerbose("End alterUserCredentials doFilter for ForceLogin");
		}
		return CONTINUE;
	}

	/**
	 * This method verbose traces output to the UCM console.
	 * 
	 * @param message
	 *            The String to trace to the UCM console.
	 */
	private void traceVerbose(final String message) {
		if (m_verbose) {
			trace(message);
		}
	}

	/**
	 * This method verbose traces output to the UCM console.
	 * 
	 * @param message
	 *            The String to trace to the UCM console.
	 */
	private void trace(final String message) {
		Report.trace(TRACE_SECTION, message, null);
	}
}
